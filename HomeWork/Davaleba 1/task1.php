<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Payroll Form</h2>
    <form  method="get">
    <label for="first_name">First Name:</label><br>
    <input type="text" name="first_name" id="first_name" required><br>

    <label for="last_name">Last Name:</label><br>
    <input type="text" name="last_name" id="last_name" require><br>

    <label for="position">Position:</label><br>
    <input type="text" name="position" id="position" require><br>

    <label for="salary_amount">Salary Amount:</label><br>
    <input type="number" name="salary_amount" id="salary_amount" require><br>

    <label for="income_percentage">Income Percentage:</label><br>
    <select name="income_percentage" id="income_percentage">
        <option value="0.20">20%</option>
        <option value="0.25">25%</option>
        <option value="0.30">30%</option>
    </select><br><br>

    <input type="submit" value="Submit">
    </form>
    
    <?php
    if($_SERVER["REQUEST_METHOD"] == "GET"){
        if(isset($_GET['first_name']) && isset($_GET['last_name']) && isset($_GET['position']) && isset($_GET['salary_amount']) && isset($_GET['income_percentage'])){
            $first_name = $_GET['first_name'];
            $last_name = $_GET['last_name'];
            $position = $_GET['position'];
            $salary_amount = $_GET['salary_amount'];
            $income_percentage = $_GET['income_percentage'];

            $withheld_income = $salary_amount * $income_percentage;
            $accrued_wages = $salary_amount - $withheld_income;


            echo "<h2>Payroll Details</h2>";
            echo "<table border='1'>";
            echo "<tr><th>FirstName</th><th>LastName</th><th>Position</th><th>Salary Amount</th><th>Withheld Income</th><th>Accrued Wages</th></tr>";
            echo "<tr><td>$first_name</td><td>$last_name</td><td>$position</td><td>salary_amount</td><td>$withheld_income</td><td>$accrued_wages</td></tr>";
            echo "</table>";
        }
    }
    ?>
</body>
</html>