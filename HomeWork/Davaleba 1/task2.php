<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form  method="post">
        <label for="student_name">Student's Name:</label><br>
        <input type="text" name="student_name" id="student_name" require><br>

        <label for="student_lastname">Student's Lastname:</label><br>
        <input type="text" name="student_lastname" id="student_lastname" require><br>

        <label for="course">Student's Course:</label><br>
        <input type="text" name="student_course" id="student_course" require><br>

        <label for="student_semester">Student's Semester:</label><br>
        <input type="number" name="student_semester" id="student_semester" require><br>

        <label for="learning_course">Learning Course:</label><br>
        <input type="number" name="learning_course" id="learning_course" require><br>

        <label for="received_mark">Received Mark:</label><br>
        <input type="number" name="received_mark" id="received_mark" require><br><br>

        <label for="lecturer_name">Lecturer's Name:</label><br>
        <input type="text" id="lecturer_name" name="lecturer_name" required><br>
        
        <label for="lecturer_surname">Lecturer's Surname:</label><br>
        <input type="text" id="lecturer_surname" name="lecturer_surname" required><br>
        
        <label for="dean_name">Dean's Name:</label><br>
        <input type="text" id="dean_name" name="dean_name" required><br>
        
        <label for="dean_surname">Dean's Surname:</label><br>
        <input type="text" id="dean_surname" name="dean_surname" required><br><br>
        
        <input type="submit" value="Submit">
    </form>


    <?php
    if($_SERVER["REQUEST_METHOD"]=="POST"){
        $student_name = $_POST['student_name'];
        $student_lastname = $_POST['student_lastname'];
        $student_course = $_POST['student_course'];
        $student_semester = $_POST['student_semester'];
        $learning_course = $_POST['learning_course'];
        $received_mark = $_POST['received_mark'];
        $lecturer_name = $_POST['lecturer_name'];
        $lecturer_surname = $_POST['lecturer_surname'];
        $dean_name = $_POST['dean_name'];
        $dean_surname = $_POST['dean_surname'];


        $grade='';

        if( $received_mark >= 90){
            $grade = 'A - Excellent';
        }elseif ($received_mark  >=80){
            $grade = 'B - Very Good';
        }elseif ($received_mark >=70){
            $grade = 'C - Good';
        }elseif ($received_mark >=60){
            $grade = 'D - Satisfactory';
        }elseif ($received_mark >=50){
            $grade = 'E - Sufficient';
        }else{
            $grade = 'F - Fail';
        }
        

        echo "<h2>Student Details</h2>";
        echo "<table border = '1'>";
        echo "<tr><th>Field</th><th>Value<th></tr>";
        echo "<tr><td>Student's Name</td><td>$student_name</td></tr>";
        echo "<tr><td>Student's Surname</td><td>$student_lastname</td></tr>";
        echo "<tr><td>Student's Course</td><td>$student_course</td></tr>";
        echo "<tr><td>Received Mark</td><td>$received_mark ($grade)</td></tr>";
        echo "<tr><td>Lecturer's Name</td><td>$lecturer_name</td></tr>";
        echo "<tr><td>Lecturer's Surname</td><td>lecturer_surname</td></tr>";
        echo "<tr><td>Dean's Name</td><td>$dean_name</td></tr>";
        echo "<tr><td>Dean's Surname</td><td>$dean_surname</td></tr>";
        echo "</table>";

    }
    ?>

    
</body>
</html>