<?php
    $name_error = $email_error = "";
    $success = true;

    if(isset($_POST['name']) && empty($_POST['name'])){
        $name_error = 'Name is required!!!';
    }

    if(isset($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $email_error = 'Email is not valid!!';
    }

    if($name_error!="" || $email_error!= ""){
        $success = false;
    }
?>
 