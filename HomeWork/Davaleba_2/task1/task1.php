<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Enter a Number</h2>
    <form  method="post">
        <label for="number">Enter a Number</label><br>
        <input type="number" id="number" name="number" required><br><br>
        <input type="submit" value="Submit">
    </form>


    <?php
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $number = $_POST['number'];
        $array = [];

        for($i=0; $i < 12;$i++){
            $array[] = 10 + ($i * 10);
        }

        $less_than_x = 0;
        $more_than_x = 0;

        foreach($array as $element){
            if($element < $number ){
                $less_than_x++;
            }elseif($element > $number){
                $more_than_x++;
            }
        }

        echo "<h2>Array Comparison Results</h2>";
        echo "Number of elements less than $number: $less_than_x<br>";
        echo "Number of elements more than $number: $more_than_x";
    }
    ?>
</body>
</html>