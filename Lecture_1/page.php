<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My First</title>
</head>
<body>
    <h1>Hello World!!!</h1>
    <form action="get.php" method="get">
        <h2>GET</h2>
        <input type="text" name="monacemi">
        <br> <br>
        <button>SEND</button>
        <br><br>
    </form>
    <hr> <hr>
    <form action="post.php" method="post">
        <h2>POST</h2>
        <input type="text" name="saxeli">
        <br> <br>
        <button>SEND</button>
        <br><br>
    </form>

    <?php
        echo "<h1>Hello PHP</h1>";
        $x = 17;
        $s = "GAU";
        echo $x;
        echo "<h2>$s</h2>";
        
        $a = [12, "Gau", 23, false];
        echo "<hr>";
        print_r($a);

        $as = [
            'name' => "Giorgi",
            'age' => 19,
            'gpa' => 3.9
        ];

        echo "<hr>";
        print_r($as);
    ?>
</body>
</html>