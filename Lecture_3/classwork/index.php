<?php
    include "array_of_subjects.php";
    include "validate.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 3</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <?php
            if(isset($_POST["student_reg"]) && $success){
                include "form_of_manager.php";
            }else{
                include "form_of_student.php";
            }   
        ?>
    </div>
</body>
</html>