<?php
    $subjects = [
        ['subject'=>"PHP", 'ects'=>6],
        ['subject'=>"C#", 'ects'=>4],
        ['subject'=>"R", 'ects'=>4],
        ['subject'=>"Angular", 'ects'=>5],
        ['subject'=>"SQL", 'ects'=>4],
        ['subject'=>"Laravel", 'ects'=>6],
        ['subject'=>"JavaScript", 'ects'=>5]
    ];
?>